# ff-userjs

My user.js file for Firefox. To load it, copy the user.js file into your Firefox
profile. The profile folder has the following locations:

* Linux & BSD: `$HOME/.mozilla/firefox/XXXXXXX.default/`
* Windows: `C:\Users\YourUserName\AppData\Roaming\Mozilla\Firefox\Profiles\XXXXXXX.default`
* Mac OS: `Users/YourUserName/Library/Application Support/Firefox/Profiles/XXXXXXX.default`
