// Allow userContent.css and userChrome.css
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
// Set UI density to normal
user_pref("browser.uidensity", 0);
// Enable SVG context-propertes
user_pref("svg.context-properties.content.enabled", true);
// Disables the Navigation Timing API for security related reasons
//user_pref("dom.enable_performance", false);
// Disables user timing for security related reasons
//user_pref("dom.enable_user_timing", false);
// Disables geolocation for security and privacy related reasons
user_pref("geo.enabled", false);
// Disbles raw TCP socket support for security reasons
user_pref("dom.mozTCPSocket.enabled", false);
// Disables Web Storage; not recommended as it can cause site breakage
//user_pref("dom.storage.enabled", false);
// Prevents the leaking of network connection info via Javascript
user_pref("dom.netinfo.enabled", false);
// Disables WebRTC to prevent leakage of local IP; will break web based video conferencing and any peer-to-peer web apps
user_pref("media.peerconnection.enabled", false);
// If WebRTC is enabled, prevents your local IP from being leaked
user_pref("media.peerconnection.ice.no_host", true);
// Disables screen sharing, audio capture, and video capture for security and privacy related reasons
user_pref("media.navigator.enabled", false);
user_pref("media.navigator.video.enabled", false);
user_pref("media.getusermedia.screensharing.enabled", false);
user_pref("media.getusermedia.audiocapture.enabled", false);
// Enforce larger tab width minimum
user_pref("browser.tabs.tabMinWidth", 100);
// Warn on close
user_pref("browser.tabs.warnOnClose", true);
// Disables the battery API for privacy related reasons
user_pref("dom.battery.enabled", false);
// Disables the telephony API
user_pref("dom.telephony.enabled", false);
// Disables asynchronous HTTP transfers for privacy related reasons
user_pref("beacon.enabled", false);
// Prevents websites from knowing what you copy into your clipboard, but disables copy/paste functionality for javascript-based web apps like Google Drive.
//user_pref("dom.event.clipboardevents.enabled", false);
// Disables speech recognition and synthesis
user_pref("media.webspeech.recognition.enable", false);
user_pref("media.webspeech.synth.enabled", false);
// Disables the sensor API
user_pref("device.sensors.enabled", false);
// Disable pinging URIs and if it is enabled, only allow pinging the same host as the origin page
user_pref("browser.send_pings", false);
user_pref("browser.send_pings.require_same_host", true);
// Disables IndexedDB for tracking purposes, but causes breakage in numerous extensions if disabled, so left disable by default
//user_pref("dom.indexedDB.enabled", false);
// Disables gamepad API to prevent usb device enumeration
user_pref("dom.gamepad.enabled", false);
// Disables VR API
user_pref("dom.vr.enabled", false);
// Disables vibrator API
user_pref("dom.vibrator.enabled", false);
// Disables resource timing for privacy related reasons
//user_pref("dom.enable_resource_timing", false);
// Disables the WebGL API for security related reasons
user_pref("webgl.disabled", true);
// Don't use Mozilla's location specific search engines
user_pref("browser.search.geoSpecificDefaults", false);
// Disables selection from being automatically copied on UNIX-like platforms
user_pref("clipboard.autocopy", false);
// Prevents http from being trimmed off of URLs in the address bar
user_pref("browser.urlbar.trimURLs", false);
// Don't guess domain names off of incomplete entries in the address bar
user_pref("browser.fixup.alternate.enabled", false);
// Send DNS requests through SOCKS if a SOCKS proxy is in use
user_pref("network.proxy.socks_remote_dns", true);
// Don't monitor OS online/offline status
user_pref("network.manage-offline-status", false);
// Enables Mixed Active Content Blocking
user_pref("security.mixed_content.block_active_content", true);
// Enables Passive Content Blocking; will prevent some media from being loaded on websites that aren't fully SSL encrypted
user_pref("security.mixed_content.block_display_content",    true);
// Disables video stats for privacy relates reasons
user_pref("media.video_stats.enabled", false);
// Prevents font fingerprinting
user_pref("browser.display.use_document_fonts", 0);
// Disables the health report
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
// Disables extension recommendations
user_pref("browser.discovery.enabled", false);
user_pref("extensions.htmlaboutaddons.recommendations.enabled", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);
// Disables Google safebrowsing
user_pref("browser.safebrowsing.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
// Disables Pocket
user_pref("extensions.pocket.enabled", false);
// Disables Shield
user_pref("app.shield.optoutstudies.enabled", false);
// Disables the UI tour for security related reasons
user_pref("browser.uitour.enabled", false);
// Enables tracking protection
user_pref("privacy.trackingprotection.enabled", true);
// Disables the PDF viewer; good for security, but you need a pdf viewer that works offline
user_pref("pdfjs.disabled", true);
// Disables a number of suggestions in the URL bar
user_pref("browser.search.suggest.enabled", false);
user_pref("browser.urlbar.suggest.searches", false);
user_pref("browser.urlbar.suggest.history", false);
user_pref("browser.urlbar.groupLabels.enabled", false);
user_pref("browser.urlbar.suggest.quicksuggest.sponsored", false);
user_pref("browser.urlbar.suggest.quicksuggest.nonsponsored", false);
// Disables speculative connecting
user_pref("network.http.speculative-parallel-limit", 0);
// Ensures that Content Security Policy is enabled
user_pref("security.csp.enable", true);
// Enable Subresource Integrity
user_pref("security.sri.enable", true);
// Only allows 1st party cookies
user_pref("network.cookie.cookieBehavior", 1);
// Restricts data in DOM storage to be accessed only by the domains which added them to DOM storage; can cause some breakage
user_pref("privacy.firstparty.isolate", true);
// Permanently enable private browsing mode
user_pref("browser.privatebrowsing.autostart", true);
// Prevents websites from downloading URLs into the cache and disables it completely (use something like Squid for caching)
user_pref("browser.cache.offline.enable", false); 
user_pref("browser.cache.disk.enable", false);
user_pref("browser.cache.disk_cache_ssl", false);
user_pref("browser.cache.disk.capacity", 0);
// Wipes history on shutdown
user_pref("privacy.sanitize.sanitizeOnShutdown", true);
user_pref("privacy.clearOnShutdown.cache", true);
user_pref("privacy.clearOnShutdown.cookies", true);
user_pref("privacy.clearOnShutdown.downloads", true);
user_pref("privacy.clearOnShutdown.formdata", true);
user_pref("privacy.clearOnShutdown.history", true);
user_pref("privacy.clearOnShutdown.offlineApps", true);
user_pref("privacy.clearOnShutdown.sessions", true);
user_pref("privacy.clearOnShutdown.openWindows", true);
// Don't remember history or download history
user_pref("places.history.enabled", false);
user_pref("browser.download.manager.retention", 0);
// Disables the built-in password manager and the autofill of forms
user_pref("signon.rememberSignons", false);
user_pref("browser.formfill.enable", false);
user_pref("extensions.formautofill.creditCards.enabled", false);
user_pref("extensions.formautofill.addresses.enabled", false);
user_pref("signon.autofillForms", false);
// Makes cookies expire at the end of the session
user_pref("network.cookie.lifetimePolicy", 2);
// I can't stand the gray lock icon to indicate ssl support, so lets make it green again
user_pref("security.secure_connection_icon_color_gray", false);
// Clears SSL form session data
user_pref("browser.sessionstore.privacy_level", 2);
// Deletes temp files on shutdown
user_pref("browser.helperApps.deleteTempFileOnExit", true);
// Always ask where to download
user_pref("browser.download.useDownloadDir", false);
// Disables cruft in the default new tab page
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
user_pref("browser.newtabpage.activity-stream.enabled", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.highlights", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);
user_pref("browser.newtabpage.activity-stream.feeds.topsites", false);
user_pref("browser.newtabpage.activity-stream.prerender", false);
// Disables the CSS :visited selectors for security reasons
user_pref("layout.css.visited_links_enabled", false);
// Disables autocomplete
user_pref("browser.urlbar.autoFill", false);
// Enables HSTS preload list 
user_pref("network.stricttransportsecurity.preloadlist", true);
// Enables OCSP and some of its features
user_pref("security.OCSP.enabled", 1);
user_pref("security.ssl.enable_ocsp_stapling", true);
user_pref("security.ssl.enable_ocsp_must_staple", true);
user_pref("security.OCSP.require", true);
// Controls the minimum and maximum versions of TLS used. The current defaults are good, so these are commented out
//user_pref("security.tls.version.min", 3);
//user_pref("security.tls.version.max", 4);
// Makes Public key pinning enforcement mandatory
user_pref("security.cert_pinning.enforcement_level", 2);
// Disallows the use of SHA1
user_pref("security.pki.sha1_enforcement_level", 1);
// Warns the user when safe renegotiation isn't supported
user_pref("security.ssl.treat_unsafe_negotiation_as_broken", true);
// Disables the automatic reporting of TLS connection errors
user_pref("security.ssl.errorReporting.automatic", false);
// Encrypts SNI
user_pref("network.security.esni.enabled", true);
// Disables insecure ciphers
user_pref("security.ssl3.dhe_rsa_aes_256_sha", false);
user_pref("security.ssl3.dhe_rsa_aes_128_sha", false);
// Enable Fission
user_pref("fission.autostart", true);
user_pref("gfx.webrender.all", true);
// Fix Vsync
user_pref("gfx.vsync.force-disable-waitforvblank", true);
// Remove Telemetry
clearPref("toolkit.telemetry.cachedClientID");
clearPref("browser.newtabpage.activity-stream.impressionId");
// Theming
user_pref("widget.non-native-theme.gtk.scrollbar.allow-buttons", true);
user_pref("widget.gtk.native-context-menus", false);
user_pref("widget.content.allow-gtk-dark-theme", true);
